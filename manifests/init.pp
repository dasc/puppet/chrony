class chrony (
    String $service_name,
    Boolean $service_enable,
    Boolean $service_hasstatus,
    String $service_ensure,
    String $package_ensure,
    String $package_name,
    String $conffile,
    String $keyfile,
    Optional[String] $allow = undef,
    Array[String] $servers,
) {
    package { $chrony::package_name:
      ensure => $chrony::package_ensure,
    }

    file { 'chrony.conf':
	path => $chrony::conffile,
        ensure => $chrony::package_ensure,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('chrony/etc/chrony.conf.epp', {
	    'servers' => $servers,
	    'keyfile' => $keyfile,
	    'allow'   => $allow,
	    }),
	require => Package["$::chrony::package_name"],
    }

    # This 'if' statement is necessary for Ubuntu's upstart, which will
    # complain if you try to disable or stop a service that doesn't exist.
    if $chrony::package_ensure != 'absent' {
	service { 'chrony':
	    enable => $chrony::service_enable,
	    ensure => $chrony::service_ensure,
	    name => $chrony::service_name,
	    hasstatus => $chrony::service_hasstatus,
	    require => Package["$::chrony::package_name"],
	    subscribe => File['chrony.conf'],
	}
    }
}
